### puppet apply r10k


# export FACTER_forge_url="<puppetforge>"
$baseurl = $forge_url ? { 
  undef => "https://forgeapi.puppetlabs.com",
  default => $forge_url,
}

# export FACTER_r10k_remote=$(git config --get remote.origin.url)
$remote = $r10k_remote 

if ! $remote {
  fail("r10k_remote must be defined. Try 'export FACTER_r10k_remote=$(git config --get remote.origin.url)'")
} 

class { 'r10k':
  include_postrun_command => false,
  version => '2.4.3',
  forge_settings => {
    #'baseurl' => 'http://puppetforge001.rtp.eng.netapp.com:8080',
    'baseurl' => $baseurl,
  },
  sources => {
    'puppet' => {
      'remote'  => $remote,
      'basedir' => "/etc/puppet/environments",
    },
  },
}

notify { 'R10K Killer Robot Sez':
  message => "Run this: r10k deploy environment -p",
}
